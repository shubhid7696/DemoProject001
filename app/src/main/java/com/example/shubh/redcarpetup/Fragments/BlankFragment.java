package com.example.shubh.redcarpetup.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.shubh.redcarpetup.Activities.MainActivity;
import com.example.shubh.redcarpetup.Classes.Display_class;
import com.example.shubh.redcarpetup.Model.Posts;
import com.example.shubh.redcarpetup.R;
import com.example.shubh.redcarpetup.Retrofit.MyApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    String responseString,image2;
    JSONObject jsonObject;
    List<Posts> list = new ArrayList<>();
    ArrayList<String> links = new ArrayList<>();
    String flag_link;
    boolean isImageFitToScreen;
    Bundle bundle;
    DisplayFullScreenImage dsp_img ;
    Display_class displayClass = new Display_class();
    int position;
    MyApi myApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        fetchData();

        return view;
    }

    private void fetchData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.androidbegin.com/tutorial/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        MyApi apiService = retrofit.create(MyApi.class);
        apiService.getPosts("http://www.androidbegin.com/tutorial/jsonparsetutorial.txt")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<ResponseBody>() {
                    @Override
                    public void onError(Throwable e) {
                        Log.d("TAHA ONERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("TAHA ONCOMPLETED", "COMPLETED TRANSFER");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            responseString = responseBody.string();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Log.d("RESPONSE STRING", responseString);

                        //Converting Response to JSONObject
                        try {
                            jsonObject = new JSONObject(responseString);
                            Log.d("TAHA", jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //getting world population.
                        JSONArray jsonArray = jsonObject.optJSONArray("worldpopulation");
                        for(int i=0; i<jsonArray.length();i++){
                            Posts posts = new Posts();
                            JSONObject jsonobject = null;
                            try {
                                jsonobject = jsonArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                String rank = jsonobject.getString("rank");
                                posts.setRank(Integer.valueOf(rank));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                String country = jsonobject.getString("country");
                                posts.setCountry(country);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                String population = jsonobject.getString("population");
                                Long l = Long.parseLong(population.replaceAll(",", ""));
                                posts.setPopulation(l);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                String image = jsonobject.getString("flag");
                                image2 = image;
                                posts.setFlag(image);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //adding data to List
                            list.add(posts);
                            links.add(image2);
                            Log.d("TAHA COUNTRY MODEL", list.toString());

                        }

                        recyclerAdapter = new RecyclerAdapter();
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(recyclerAdapter);
                        recyclerAdapter.notifyDataSetChanged();
                    }
                });
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.post_layout,viewGroup,false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int i) {
            isImageFitToScreen = false;
            dsp_img = new DisplayFullScreenImage();
            bundle = new Bundle();

            position = i;

            holder.rank.setText(String.valueOf(list.get(i).getRank()));
            holder.country.setText(String.valueOf(list.get(i).getCountry()));
            holder.population.setText("POPULATION: "+String.valueOf(list.get(i).getPopulation()));
            flag_link = String.valueOf(String.valueOf(list.get(i).getFlag()));
            Glide.with(getActivity()).load(String.valueOf(list.get(i).getFlag()))
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.INVISIBLE);
                            holder.imageView.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(holder.imageView);

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "Item "+i+" clicked", Toast.LENGTH_SHORT).show();

                    bundle.putInt("Position",position);
                    bundle.putStringArrayList("Image_Link",links);
                    dsp_img.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,dsp_img).commit();
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;
            ProgressBar progressBar;
            TextView rank,country,population;
            RelativeLayout relativeLayout;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imageView);
                progressBar = itemView.findViewById(R.id.progressBar);
                rank = itemView.findViewById(R.id.text_rank);
                country = itemView.findViewById(R.id.text_country);
                population = itemView.findViewById(R.id.text_population);

            }
        }
    }
}

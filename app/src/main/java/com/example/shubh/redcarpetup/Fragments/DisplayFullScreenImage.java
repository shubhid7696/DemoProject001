package com.example.shubh.redcarpetup.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.shubh.redcarpetup.Classes.Display_class;
import com.example.shubh.redcarpetup.Model.Posts;
import com.example.shubh.redcarpetup.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DisplayFullScreenImage extends Fragment {

    RecyclerView rcr;
    Display_class dsp;
    fullScreenRecyclerAdapter fsir;
    ArrayList<String> img_link = new ArrayList<>();

    int pos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_display_full_screen_image, container, false);

        dsp = new Display_class();

        pos=getArguments().getInt("Position");
        pos = pos-1;
        if(pos<1){
            pos=0;
        }
        img_link = getArguments().getStringArrayList("Image_Link");

        rcr = view.findViewById(R.id.recycler_view_full_scr);
        rcr.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        fsir = new fullScreenRecyclerAdapter();  //adapter for recycler view
        rcr.setAdapter(fsir);
        fsir.notifyDataSetChanged();  // update the adapter.

        rcr.scrollToPosition(pos);

        return view;
    }

    public class fullScreenRecyclerAdapter extends RecyclerView.Adapter<fullScreenRecyclerAdapter.My_ViewHolder> {

        @Override
        public My_ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.full_screen_imageview,viewGroup,false);
            return new My_ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(My_ViewHolder holder, int i) {
            Picasso.with(getActivity())
                    .load(img_link.get(i))
                    .into(holder.imv);

            //to implement double tap or pinch zoom.
            holder.imv.setOnTouchListener(new ImageMatrixTouchHandler(getActivity()));
        }

        @Override
        public int getItemCount() {
            return img_link.size();
        }

        public class My_ViewHolder extends RecyclerView.ViewHolder {
            ImageView imv;
            public My_ViewHolder(@NonNull View itemView) {
                super(itemView);
                imv = itemView.findViewById(R.id.full_scr_img_view);
            }
        }
    }
}

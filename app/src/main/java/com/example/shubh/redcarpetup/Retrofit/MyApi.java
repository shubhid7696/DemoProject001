package com.example.shubh.redcarpetup.Retrofit;

import com.example.shubh.redcarpetup.Model.Posts;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface MyApi {

    @Streaming
    @GET
    Observable<ResponseBody> getPosts(@Url String url);


}
